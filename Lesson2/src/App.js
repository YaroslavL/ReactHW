import React, { Component } from 'react';
import './main.css';

import ComponentsType from './ComponentsType/';
import ChildElements from './ChildElements/';
import Lifecycle from './Lifecycle/';
import BindDemo from './Binding/';
import Task1 from './task1';
import Task2 from './task2';

class App extends Component {

  constructor( props ){
    super(props);

    this.state = {
      showBlocks: [
        {
          title: 'Lets talk about stateful and stateless components',
          visibility: false,
          component: ComponentsType
        },
        {
          title: 'Components childrens',
          visibility: false,
          component: ChildElements
        },
        {
          title: 'How to bind functions to component state',
          visibility: false,
          component: BindDemo
        },
        {
          title: 'Component Lifecycle',
          visibility: true,
          component: Lifecycle
        }
      ]
    };

    // this.stateStuff = this.stateStuff.bind(this);
  }


  actionClick = () => { alert("OK")};

  render() {
    // Деконструкция стейта
    let {renderBlocks, showBlocks} = this.state;
    let { actionClick } = this;
    return (
      <div className="App">
      <div className="LessonHeader">
        <h1>Lesson 2. Statefull and Stateless components. Components with childrens</h1>
      </div>
      <div className="wrap">
        {
          showBlocks.map( (item, key) => {
            let Component = item.component;
            // return item.visibility === true ?  : <div></div>
            if( item.visibility == true ){
              return( <Component key={key} title={item.title}/> )
            } else {
              return null
            }
          })
        }
        </div>
        <div>
          <Task1
            style={{
              backgroundColor: "#ffcc00"
            }}
            text="test"
            action={actionClick}

            />
        </div>
        <div>
          <Task2/>
        </div>

      </div>
    );
  }
}


export default App;
