import React from 'react';

const StateLess = ({style, text, action, data}) => {


  return(
    <button style={style} onClick={action} data={data}>
        {text}
    </button>
  )

}

export default StateLess;
