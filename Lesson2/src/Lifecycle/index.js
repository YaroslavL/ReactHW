import React, { Component } from 'react';


/*

  Давайте разберем React Component Lifecycle
  https://reactjs.org/docs/react-component.html#lifecycle-methods
  http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/
*/
class Lifecycle extends Component {
  /*
    Блок 1.
    Инициализация компонента.
  */
  constructor( props ){
    super(props); // Вызываем конструктор реакта передавая туда props
    /*
      В конструктор обьявляется state компонента.
      Так же, тут биндядся обработчики событий к классу
    */
    console.log('0.constructor');
    this.state = {  /* ... */  };

    // Бинд необходим для того что бы их функции можно было получить доступк к нативному методу реката
    // this.setState({});
    this.myMethod = this.myMethod.bind(this);
    this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
  }
  myMethod(){ this.setState({/* ... */});}
  forceUpdateHandler(){  this.forceUpdate(); }
  componentWillMount(){
    // Выполняется перед тем ка компонент будет смонтирован.
    // Метод выполняется на сервере, при наличии SSR.

    // Рекомендация с доков -> DR.
    // Не использовать сайд-эффекты в этом методе.
    // Сайд эффект - любая функция которая приводит к изменению стейта приложения
    // Вызов setState() в этом методе НЕ вызовет перерисовку, так как выполняется .
    console.log('1.componentWillMount');
  }
  // Тут вызывается рендер, между will & did.

  componentDidMount(){
    // Выполняется после первой отрисовки.
    // Считается лучшим местом, для того что бы инициализировать загрузку данных с сервера.
    // Вызов setState() в этом методе вызовет перерисовку.
    // API.Get(...)
    let requestUrl = "http://www.json-generator.com/api/json/get/ckPDzNHDAO?indent=2";
    fetch(requestUrl).then(
      res => res.json()
    ).then(
      data => {

        let transformedArray = data.map( user => {
          return({
            interviewed: false,
            user
          });
        });
        console.log( 'indexOf', transformedArray.indexOf({
          interviewed: false,
          user:{
            eyeColor:"blue",
            index: 2,
            name:"Kristine Stone"
          }
        }) );
        this.setState({transformedArray});
      }
    );
    console.log('3.componentDidMount');
  }

  /*
    Блок 2.
    Изменение данных компонента
  */

  componentWillReceiveProps(nextProps){
    /*
      Метод вызывается прямо перед тем, как получить новые props.
      Если нужно сравнить новые и старые пропсы и на основе результата провести манипуляцию,
      в результате которой будет изменен стейт, то документация рекоментудет сделать это тут!

      Метод не вызывается при инициализации.
      Изменение state игнорирует это событие. Работает только с props.
    */
    console.log('4.componentWillReceiveProps', nextProps);
  }

  shouldComponentUpdate(nextProps, nextState){
    /*
      Тут проверяется, является ли изменение состояние или пропсов причиной
      перерисовки всего компонента.
      Должен возвращать boolean -> true/false

      Если возвращает false, то методы componentWillUpdate(), render(), и componentDidUpdate()
      не сработают.
    */
    console.log('5.shouldComponentUpdate', nextProps, nextState);
    if( nextProps.updatedProps === 1223){
        return false;
    } else {
        return true;
    }
  }

  componentWillUpdate(nextProps, nextState){
    /*
      В теле этого метода, нельзя вызвать  this.setState().
      Потому что это вызовет перерисовку до оконцания этой функции.

      Не сработает, если shouldComponentUpdate вернул false.
    */
    console.log('6.componentWillUpdate');
  }
  // Тут снова Рендер.
  componentDidUpdate(prevProps, prevState){

    /*
      Срабатывает сразу после рендера данных.
      Так же как и componentDidMount является отличным местом для side effects.

    */
    console.log('8.componentDidUpdate');
  }
  /*
    Блок 3.

  */
  componentWillUnmount(){
    /*
      Вызывается прямо перед тем, как компонент будет удален из DOM
    */
    console.log('7.componentWillUnmount');
  }

  render(){
    console.log('2.render');
    return(
      <div>
        <button className="functionButton" onClick={this.props.outsideDataUpdate}>Lifecycle</button>
        <button className="functionButton" onClick= {this.forceUpdateHandler} >FORCE UPDATE</button>
        <h4>Random Number : { Math.random() }</h4>
      </div>
    );
  }
}


export default Lifecycle;
